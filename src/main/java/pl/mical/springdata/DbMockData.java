package pl.mical.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.mical.springdata.repository.CustomerRepository;
import pl.mical.springdata.repository.OrderRepository;
import pl.mical.springdata.repository.ProductRepository;
import pl.mical.springdata.repository.UserRepository;
import pl.mical.springdata.repository.entity.Customer;
import pl.mical.springdata.repository.entity.Order;
import pl.mical.springdata.repository.entity.Product;
import pl.mical.springdata.service.UserService;
import pl.mical.springdata.user.User;
import pl.mical.springdata.user.UserDto;
import pl.mical.springdata.user.UserDtoBuilder;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepository productRepository;
    private OrderRepository orderRepository;
    private CustomerRepository customerRepository;


    @Autowired
    public DbMockData(ProductRepository productRepository, OrderRepository orderRepository, CustomerRepository customerRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;

    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Korek", 2.55f, true);
        Product productDwa = new Product("Rura", 5f, true);
        Customer customer = new Customer("Jak Kowalski", "Wrocław");
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(productDwa);
            }};
        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");

        productRepository.save(product);
        productRepository.save(productDwa);
        customerRepository.save(customer);
        orderRepository.save(order);

        Product product1 = new Product("sok", 2.50f, true);
        productRepository.save(product1);
        Product product2 = new Product("woda", 1f, true);
        productRepository.save(product2);
        Product product3 = new Product("cola", 1000f, false);
        productRepository.save(product3);
    }
}
