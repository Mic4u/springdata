package pl.mical.springdata.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mical.springdata.repository.entity.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
