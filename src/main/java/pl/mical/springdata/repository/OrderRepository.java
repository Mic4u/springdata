package pl.mical.springdata.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mical.springdata.repository.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
}
