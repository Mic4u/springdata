package pl.mical.springdata.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mical.springdata.repository.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
}
