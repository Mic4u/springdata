package pl.mical.springdata.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mical.springdata.user.UserDto;

@Repository
public interface UserRepository extends CrudRepository<UserDto, Long> {
}
