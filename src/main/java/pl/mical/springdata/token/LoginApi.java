package pl.mical.springdata.token;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.mical.springdata.service.UserService;
import pl.mical.springdata.user.PasswordEncoderConfig;
import pl.mical.springdata.user.User;
import pl.mical.springdata.user.UserDto;

import java.util.Date;
import java.util.Optional;


@RestController
public class LoginApi {

    private UserService userService;
    private static PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();

    @Autowired
    public LoginApi(PasswordEncoderConfig passwordEncoderConfig, UserService userManager) {
        this.passwordEncoderConfig = passwordEncoderConfig;
        this.userService = userManager;
    }

    private String getRole(String name) {

        Optional<UserDto> userOpt  = userService.getByName(name);
        UserDto userDto = userOpt.get();
        String role = userDto.getRole();
        return role;

    }

    private boolean validate(User user) {
        String pass = user.getPassword();

        Optional<UserDto> userOpt  = userService.getByName(user.getName());
        UserDto userDto = userOpt.get();
        String passwordHash = userDto.getPasswordHash();

        return !passwordEncoderConfig.passwordEncoder().matches(pass, passwordHash);
    }

@PostMapping("/getToken")
    public String login(@RequestBody User user) {

    long timeMiliseconds = System.currentTimeMillis();
    String name = user.getName();


    if (validate(user)) { // if wrong credentials
        return "401 Unauthorized";
    } else {

        return Jwts.builder()
                .setSubject(user.getName())
                .claim("role", getRole(name))
                .setIssuedAt(new Date(timeMiliseconds))
                .setExpiration(new Date(timeMiliseconds + 30000))
                .signWith(SignatureAlgorithm.HS256, "L1tW4")
                .compact();
    }
}

}
