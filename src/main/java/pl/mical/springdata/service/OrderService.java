package pl.mical.springdata.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.mical.springdata.repository.CustomerRepository;
import pl.mical.springdata.repository.OrderRepository;
import pl.mical.springdata.repository.ProductRepository;
import pl.mical.springdata.repository.entity.Customer;
import pl.mical.springdata.repository.entity.Order;
import pl.mical.springdata.repository.entity.Product;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class OrderService {

    CustomerRepository customerRepository;
    ProductRepository productRepository;
    OrderRepository orderRepository;

    public OrderService(CustomerRepository customerRepository, ProductRepository productRepository, OrderRepository orderRepository) {
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
    }

    public Optional<Order> getOrderById(long id) {
        return orderRepository.findById(id);
    }

    public Iterable<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public ResponseEntity addOrder(Map<String, Object> elementsOfOrders) {
        if (!elementsOfOrders.containsKey("id"))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        if (!elementsOfOrders.containsKey("products"))
            ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        if (!elementsOfOrders.containsKey("placeDate"))
            ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        if (!elementsOfOrders.containsKey("status"))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        return protectionAgainstInvalidData(new Order(), elementsOfOrders);
    }

    public ResponseEntity updateAllOrPartialOrder(Map<String, Object> updates, long id) {
        Optional<Order> orderFromDB = orderRepository.findById(id);

        return protectionAgainstInvalidData(orderFromDB.get(), updates);
    }

    private ResponseEntity protectionAgainstInvalidData(Order order, Map<String, Object> updates) {
        if (updates.containsKey("idCustomer")) {
            ArrayList<Integer> idCustomer = (ArrayList<Integer>) updates.get("idCustomer");
            if (idCustomer.size() > 1)
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
            Optional<Customer> customerFromDB = customerRepository.findById(Long.valueOf(idCustomer.get(0)));
            if (customerFromDB.isPresent())
                order.setCustomer(customerFromDB.get());
            else
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
        if (updates.containsKey("idProducts")) {
            ArrayList<Integer> idProducts = (ArrayList<Integer>) updates.get("idProducts");
            Set<Product> products = new HashSet<>();
            boolean areProducts = false;

            for (Integer index : idProducts) {
                if (productRepository.findById(Long.valueOf(index)).isPresent()) {
                    areProducts = true;
                    products.add(productRepository.findById(Long.valueOf(index)).get());
                } else {
                    areProducts = false;
                    break;
                }
            }

            if (areProducts)
                order.setProducts(products);
            else
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
        if (updates.containsKey("placeData"))
            order.setPlaceDate(LocalDateTime.parse((CharSequence) updates.get("placeData")));
        if (updates.containsKey("status")) order.setStatus((String) updates.get("status"));

        return ResponseEntity.ok(orderRepository.save(order));
    }
}

