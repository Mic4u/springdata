package pl.mical.springdata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.mical.springdata.repository.UserRepository;
import pl.mical.springdata.user.User;
import pl.mical.springdata.user.UserDto;
import pl.mical.springdata.user.UserDtoBuilder;
import java.util.Optional;


@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<UserDto> getUser(long id) {
        return userRepository.findById(id);
    }

    public Iterable<UserDto> getAll() {
        return userRepository.findAll();
    }

    public UserDto addUser(UserDto userDto) {
        return userRepository.save(userDto);
    }

    public Optional<UserDto> getByName(String name) {

        Iterable<UserDto> all = getAll();
        Optional<UserDto> result = Optional.of(new UserDto());

        for (UserDto userDto : all) {
            if (userDto.getName().equals(name)) {
                result = Optional.of(userDto);
            }
        }

        return result;

    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {


        if (getUser(1L).isEmpty()) {
            User user1 = new User("Krzysiek", "pass", "ROLE_CUSTOMER");
            User user2 = new User("Kamil", "pass", "ROLE_ADMIN");

            UserDto userDto1 = UserDtoBuilder.addNewUser(user1);
            UserDto userDto2 = UserDtoBuilder.addNewUser(user2);

            addUser(userDto1);
            addUser(userDto2);
        }

    }
}