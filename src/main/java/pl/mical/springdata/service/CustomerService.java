package pl.mical.springdata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mical.springdata.repository.CustomerRepository;
import pl.mical.springdata.repository.entity.Customer;

import java.util.Map;
import java.util.Optional;

@Service
public class   CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Optional<Customer> getCustomer(long id) {
        return customerRepository.findById(id);
    }

    public Iterable<Customer> getAll() {
        return customerRepository.findAll();
    }

    public Customer addCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(Customer customer, long id) {
        Optional<Customer> customerFromDB = customerRepository.findById(id);
        customerFromDB.get().setAddress(customer.getAddress());
        customerFromDB.get().setName(customer.getName());

        return customerRepository.save(customerFromDB.get());
    }

    public void updatePartialCustomer(Map<String, Object> updates, long id) {
        Optional<Customer> customerFromDB = customerRepository.findById(id);

        partialUpdates(customerFromDB.get(), updates);
    }

    private void partialUpdates(Customer customer, Map<String, Object> updates) {
        if (updates.containsKey("name")) customer.setName((String) updates.get("nameCustomer"));
        if (updates.containsKey("address")) customer.setAddress((String) updates.get("address"));

        customerRepository.save(customer);
    }
}