package pl.mical.springdata.user;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import pl.mical.springdata.token.JwtFilter;

@Configuration
public class AccessConfig extends WebSecurityConfigurerAdapter {

    public AccessConfig() {
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.POST, "/getToken");
        web.ignoring().antMatchers(HttpMethod.GET, "/api/product/*");
        web.ignoring().antMatchers(HttpMethod.GET, "/api/order/*");
        web.ignoring().antMatchers(HttpMethod.POST, "/api/admin/order");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()

                .antMatchers("/api/customer/*").hasRole("CUSTOMER")
                .antMatchers(HttpMethod.POST,"/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/order").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/order").hasRole("ADMIN")
                .and().httpBasic()
                .and().csrf().disable()
                .addFilter(new JwtFilter(authenticationManager()));
    }

}