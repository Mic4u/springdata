package pl.mical.springdata.user;

import org.springframework.beans.factory.annotation.Autowired;

public class UserDtoBuilder {
    private static PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();

    @Autowired
    public UserDtoBuilder(PasswordEncoderConfig passwordEncoderConfig) {
        UserDtoBuilder.passwordEncoderConfig = passwordEncoderConfig;
    }

    public static UserDto addNewUser(User user) {
        String name = user.getName();
        String encryptedPass = passwordEncoderConfig.passwordEncoder().encode(user.getPassword());
        String role = user.getRole();

        return new UserDto(name, encryptedPass, role);
    }
}
